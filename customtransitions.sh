#!/bin/bash

# Arguments
# $1    >   Directory that contains the frames for the animation loop
# $2    >   widthxheight of frame
# $3    >   Chroma colour to use (e.g.: #0f0
# $4    >   Number of frames the transition will take 

mkdir -p transition/frames
mkdir transition/animation

f_w=`expr match "$2" '\(.[0-9]*\)'`         # Clip's frame dimensions
f_h=`expr match "$2" '.*x\(.[0-9]*\)'`

for i in $1/*.png                            # Resize animation to fit frame height
    do
        convert $i -resize x$f_h transition/animation/${i##*/}
done

anim_frames=`ls $1|wc -l`                   # Count number of frames in animtion loop

cc=(transition/animation/*.png)             # Read animation loop frames into array

color=$3                                    # Chroma color to use
fs=$4                                       # Number of frames

i_w=0                                       # Find widest frame in animation loop

for i in $1/*.png
    do
    cfs=`convert $i -format "%w" info:`
    
    if [ $cfs -gt $i_w ]
        then 
            i_w=$cfs
    fi
done
        
i_s=$[(($f_w+$i_w)/$fs)+1]                  # Increment step (how much the animation must move to the left in each frame)
f_l_e=$[($i_s*$fs)-$f_w]                    # Frame left edge
p_p=$[$i_s*$fs]                             # Animation's start position
pf_w=$[($i_s*$fs)+$i_w+1]                   # Protoframe width

echo $pf_w

f_c=0                                       # Frame counter
for i in $(seq -f "%03g" 0 $fs)
do

    np=$[$p_p-(10#$i*$i_s)]    
    
    # Draw animation frame into clip frame
    convert -size ${pf_w}x${f_h} xc:none ${cc[$f_c]} -geometry +${np}+0 +antialias -composite transition/frames/frameNF.png
    
    # Fill chroma color to the right    
    convert transition/frames/frameNF.png -fill $color -floodfill +$[$pf_w-1]+1 '#00000000' transition/frames/frameUC.png
        
    # crop regular frame     
    convert transition/frames/frameNF.png -crop ${f_w}x${f_h}+$f_l_e+0 transition/frames/frame$i.png
    
    # crop chroma frame
    convert transition/frames/frameUC.png -crop ${f_w}x${f_h}+$f_l_e+0 transition/frames/chroma$i.png
    
    # make mask frame chroma
    convert transition/frames/chroma$i.png -fill black +opaque none transition/frames/mask$i.png

    # Increment frame count
    let f_c++
    
    if [ $f_c -gt $((anim_frames-1)) ]
        then
            f_c=0
    fi
    
done

rm transition/frames/frameNF.png
rm transition/frames/frameUC.png
